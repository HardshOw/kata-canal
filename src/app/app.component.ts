import { Component, Input } from '@angular/core';
import { ApiService } from './api.service';
import { map } from 'rxjs/operators';
import { Movie } from './movie'
import { EditModalComponent } from './edit-modal/edit-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [ApiService]
})
export class AppComponent {

  @Input() primaryTitle: string;
  @Input() originalTitle: string;
  @Input() startYear: Number;
  @Input() newGenre: string;
  updateMovie: Movie;
  currentMovie: Movie;
  result: Movie[]
  constructor(
    private apiService: ApiService,
    private modalService: NgbModal
    ) {
    }
  
  ngOnInit () {
  }
  public open(content, movie) {
    this.currentMovie = Movie.fromMovie(movie);
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'})
  }

  public handleSaved () {
    this.searchMovie()
    this.modalService.dismissAll();
    // Success notification
    console.log('Movie correctly updated !')
  }

  public handleClose () {
    console.log('close')
    this.modalService.dismissAll();
  }

  public searchMovie () {
    this.apiService.getMovies().pipe(
      map((movies: Movie[]) => movies.filter((movie: Movie) => {
        if (this.primaryTitle && !movie.primaryTitle.toLowerCase().match(this.primaryTitle)) { return false }
        if (this.originalTitle && !movie.originalTitle.toLowerCase().match(this.originalTitle)) { return false }
        if (this.startYear && movie.startYear !== JSON.stringify(this.startYear)) { return false }
          return true
        })
      )
    ).subscribe(response => {
      this.result = response
    })
  }
}
