import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../environments/environment';
import { map } from 'rxjs/operators';
import { Movie } from './movie'
const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor( private http: HttpClient ) { }

  public getMovies(): Observable<Movie[]> {
    return this.http
    .get(`${API_URL}/movies`).pipe(
      map(
        (jsonArray: Object[]) => jsonArray.map(jsonItem => Movie.fromJson(jsonItem))
      )
    )
  }

  public updateMovie(movie: Movie) {
    return this.http
      .patch(`${API_URL}/movies/${movie.tconst}`, movie)
  }

  private handleError (error: Response | any) {
    console.error('ApiService::handleError', error);
    return error;
  }
}
