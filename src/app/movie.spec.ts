import { Movie } from './movie';

describe('Movie', () => {
  it('should create an instance', () => {
    expect(new Movie()).toBeTruthy();
  });
});

describe('Movie', () => {
  it('should create an instance from Json', () => {
    expect(
      Movie.fromJson({
        "tconst": "tt0000004",
        "titleType": "short",
        "primaryTitle": "Un bon bock",
        "originalTitle": "Un bon bock",
        "isAdult": "0",
        "startYear": "1892",
        "endYear": "\\N",
        "runtimeMinutes": "\\N",
        "genres": "Animation,Short"
      })
    ).toBeTruthy();
  });
});

describe('Movie', () => {
  it('should create an instance from another Movie', () => {
    let movie = Movie.fromJson({
      "tconst": "tt0000004",
      "titleType": "short",
      "primaryTitle": "Un bon bock",
      "originalTitle": "Un bon bock",
      "isAdult": "0",
      "startYear": "1892",
      "endYear": "\\N",
      "runtimeMinutes": "\\N",
      "genres": "Animation,Short"
    })
    expect(Movie.fromMovie(movie)).toBeTruthy();
  });
});
