import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Movie } from '../movie'
import { ApiService } from '../api.service';

@Component({
  selector: 'app-edit-modal',
  templateUrl: './edit-modal.component.html',
  styleUrls: ['./edit-modal.component.scss'],
  providers: [ApiService]
})
export class EditModalComponent implements OnInit {

  @Input() movie: Movie
  @Input() newGenre: string;
  @Output() saved: EventEmitter<string> = new EventEmitter<string>();
  @Output() close: EventEmitter<string> = new EventEmitter<string>();

  constructor(
    private apiService: ApiService
  ) { }

  ngOnInit() {
    console.log(this.movie)
  }

  public addNewGenre () {
    if (this.movie.genres.length > 0) { this.movie.genres += ","; }
    this.movie.genres += this.newGenre;
    this.newGenre = '';
  }

  public removeGenre (genre) {
    let index = this.movie.genres.split(',').findIndex( g => g === genre)
    let genreArray = this.movie.genres.split(',')
    genreArray.splice(index, 1)
    this.movie.genres = genreArray.join(',')
  }
  public emitClose () {
    this.close.emit()
  }

  public saveMovie (movie) {
    this.apiService.updateMovie(Movie.fromJson(movie))
    .subscribe(() => {
      this.saved.emit();
    })
  }

}
