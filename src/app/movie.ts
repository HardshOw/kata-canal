export class Movie {
    // create movie from json
    public static fromJson(json: Object): Movie {
        return new Movie(
            json['tconst'],
            json['titleType'],
            json['primaryTitle'],
            json['originalTitle'],
            json['isAdult'],
            json['startYear'],
            json['endYear'],
            json['runtimeMinutes'],
            json['genres']
        );
    }
    // Create movie from another movie
    public static fromMovie(movie: Movie): Movie {
        return new Movie(
            movie.tconst,
            movie.titleType,
            movie.primaryTitle,
            movie.originalTitle,
            movie.isAdult,
            movie.startYear,
            movie.endYear,
            movie.runtimeMinutes,
            movie.genres
        );
    }
    constructor(
        public tconst: string = '',
        public titleType: string = '',
        public primaryTitle: string = '',
        public originalTitle: string = '',
        public isAdult: string = '',
        public startYear: string = '',
        public endYear: string = '',
        public runtimeMinutes: string = '',
        public genres: string = ''
    ) {}
}
