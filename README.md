# KataCanal

Ce projet a été géneré avec [Angular CLI](https://github.com/angular/angular-cli) version 8.3.23.

## Introducing me !

Bonjour ! Je m'appele Florent, je suis Developpeur Web Front-End et un javascript Lover !
Cela fait maintenant 4 ans que j'explore le framework [Vue.js](https://vuejs.org), mais, aujourd'hui voici ma toute première application [Angular](https://angular.io).
Ci-dessous vous trouverez un compte-rendu de mon ressenti pendant cette experience, les outils, les différentes libraries utilisées, ainsi que les éventuels améliorations que j'aurais pu apporter au projet avec un peu plus de temps.

## Outils, libraries et modules

- [ngBootstrap](https://ng-bootstrap.github.io/#/home)
- [Bootstrap](https://getbootstrap.com)
- [SCSS](https://sass-lang.com/documentation)
- [json-server](https://github.com/typicode/json-server)
- [Rx.js](https://rxjs-dev.firebaseapp.com) (observable via le service HttpClient d'angular)

## API / Data

Le jeu de données d'origine est accessible depuis : [Dataset](https://datasets.imdbws.com/title.basics.tsv.gz)

Pour ce kata j'ai choisi de travailler sur un échantillon de ce dataset que j'ai converti au format `JSON`.

J'ai découvert json-server pendant ce kata. [json-server](https://github.com/typicode/json-server) permet de rapidement créer une "api rest" en se basant sur un fichier json.

Le fichier utilisé par l'api [json-server](https://github.com/typicode/json-server) se situe au chemin suivant : `src/API/db.json`

Lancez le serveur API depuis la racine du projet avec la commande :

`npm run json-server`

## Compte rendu / Retro

Il s'agit de ma toute première approche avec le framework Angular et TypeScript !
J'ai essayé d'adopter le workflow et la philosophie Angular du mieux possible.
En me penchant sur le fonctionement d'angular, son integration et son panel d'outils. J'y ai retrouvé beaucoup de similitudes avec Vue.js. Certains conceptes m'étaient donc familiers. Ce qui m'as permis de retrouver certains repères côté template.

Cependant ce fut pour moi l'occasion de m'initier à la programmation réactive, et à la library Rx.js.

Le Kata nous invite à interagir avec un jeu de données.
Je me suis donc renseigner sur les differentes manières d'interagir avec une api sur angular.
J'ai donc fait la rencontre du module HttpClient.
Ce module, comme bien d'autre, utilise la library Rx.JS. Et plus précisément les objets Observables.

Angular vient avec un outil de tests unitaires et de tests End-to-end.
Respectivement [Karma](https://karma-runner.github.io) et e2e qui sont proposés par le Angular-CLI.
Je n'avais jamais eu l'occasion d'employer un de ces deux outils.
En m'inspirant des quelques exemples de codes générés par le CLI d'angular, j'ai experimenté les tests Karma.

Dans l'ensemble je suis très satisfait d'avoir fait ce Kata car il m'a permis d'approcher le framework angular et de m'en faire une opinion. Même si celà m'as pris beaucoup de temps sur un delais très court, j'en retire une éxpérience très positive, j'ai beaucoup appris !

### Avec un peu plus de temps ?

- [Karma](https://karma-runner.github.io)

  > Avec un peu plus de temps j'aurais pu approfondir mes tests unitaires. Tester toutes les méthodes et propriétés de class ou composant afin de maitriser le type qu'elles sont supposées être ou renvoyer.

- e2e

  > Configurer les tests e2e (end-to-end).

- [SCSS](https://sass-lang.com/documentation)

  > Je pourrais aussi proposer une version responsive de mon application, avec un design plus travaillé et utilisant les possibilitées qu'offre le preprocesseur css "Sass".
  > Mixins css, variables css etc ..

- Ergonomie
  > Je pourrais ameliorer l'ergonomie et l'experience utilisateur en rajoutant une notification à la sauvegarde ou en cas d'erreur.

## Commandes

### Installation

> Lancer la commande `npm i` depuis la racine du projet, pour installer toute les dépendances.

### Development server

> Lancez la commande `ng serve` pour executer le serveur de developpement. Naviguez sur l'url `http://localhost:4200/`. L'application se refresh automatiquement en cas de changement sur un fichier.

### Rest API local server (json-server)

> Serveur creer avec [json-server](https://github.com/typicode/json-server).
> Lancer le serveur API depuis la racine du projet avec la commande :
> `npm run json-server`

### Build

> Lancer la commande `ng build` pour build le projet. Le build du projet se situe au chemin suivant `dist/`.

### Running unit tests

> Lancer la commande `ng test` pour executer les tests unitaires via [Karma](https://karma-runner.github.io).

### Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
